﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JsonCsharp {
   public class Json<T> {
        public static String toString(Object obj) {
            return JsonMapper.ToJson(JsonMapper.ToValue(obj, false));
        }
        public static T toObject(String jsonString) {
            return JsonMapper.ToObject<T>(jsonString, false); 
        }
    }
}
