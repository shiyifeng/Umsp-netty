
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class JavaScriptEngine {
    public static class User{
        public String name = "name";
        public int age = 18;
    }

   public static String log(String name) {
        System.out.format("[info], %s", name);
        return "greetings from java";
    }
    public static void main(String[] args) throws Exception {
        ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
        engine.eval(new FileReader("D:\\git\\Umsp\\messagequeue\\kafkaclient\\src\\script.js"));

        Invocable invocable = (Invocable) engine;

        Object result = invocable.invokeFunction("fun1", "Peter Parker");
        System.out.println(result);
        System.out.println(result.getClass());
        System.out.println(invocable.invokeFunction("fun2", new User()));

    }
}
