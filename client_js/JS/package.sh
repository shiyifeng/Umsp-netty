#!/usr/bin/env bash
# ./package.sh

targetmin="../out/umsp.min.js"

if [ "$1" == "--weixin" ] ; then
    target="../out/umsp.js"
else
    target="../out/umsp.js"
fi

echo '' > $target
echo '' > $targetmin

function append() {
    echo "package file $1"
    echo "/* ================ $1 ================= */" >> $target || exit 1
    cat $1 >> $target
}

append "base64.js"
append "md5.js"
append "format.js"
append "config.js"
append "msutil.js"
append "uiutil.js"
append "umspdefine.js"
append "umspprotocol.js"
append "websocketclient.js"
append "websocketclientegret.js"
append "websocketclientegretnative.js"
append "websocketclientwx.js"
append "umspclient.js"

echo "

try {
  if (module && module.exports) {
     module.exports = { User, Match};
	}  
} catch (error) {
    console.log(error);
}

window.User  = User ;
window.Match  = Match ;

" >> $target



cat $target >> $targetmin

echo ""
echo ""
echo "success! "
echo "out file: $target"
echo "out file: $targetmin "
