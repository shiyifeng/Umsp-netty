/**
 * 引用android日志系统
 * @authority Ge Liang
 */

#ifndef __COMMON_H__
#define __COMMON_H__

#if !(_WIN32)
#include <unistd.h>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define LOG_TAG "Umsp-C"
#define LOGABLE
#ifdef LOGABLE
#define null NULL
#if defined(ANDROID)
	#include <android/log.h>
	#define LOGI(message, ...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, " %s) "message, __FUNCTION__, ##__VA_ARGS__)
	#define LOGW(message, ...) __android_log_print(ANDROID_LOG_WARN, LOG_TAG, " %s) "message , __FUNCTION__, ##__VA_ARGS__)
	#define LOGE(message, ...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG, " %s) "message , __FUNCTION__, ##__VA_ARGS__)
#elif (defined(_WIN32))
	#define LOGI(message, ...) printf("\n%s [I]    " message,__FUNCTION__ ,##__VA_ARGS__);fflush(stdout)
	#define LOGW(message, ...) printf("\n%s [W]    " message,__FUNCTION__ ,##__VA_ARGS__);fflush(stdout)
	#define LOGE(message, ...) printf("\n%s [E]    " message,__FUNCTION__ ,##__VA_ARGS__);fflush(stdout)
#else
	#define LOGI(message, ...) printf("\n%s [I]    " message,__FUNCTION__ ,##__VA_ARGS__);fflush(stdout)
	#define LOGW(message, ...) printf("\n%s [W]    " message,__FUNCTION__ ,##__VA_ARGS__);fflush(stdout)
	#define LOGE(message, ...) printf("\n%s [E]    " message,__FUNCTION__ ,##__VA_ARGS__);fflush(stdout)
#endif
#else
#define LOGI(message, ...) NULL
#define LOGW(message, ...) NULL
#define LOGE(message, ...) NULL
#endif


#ifdef _WIN32
#define EXPORT __declspec(dllexport)
#else
#define EXPORT __attribute__((visibility("default")))
#endif



#if _MSC_VER // this is defined when compiling with Visual Studio
#define EXPORT_API __declspec(dllexport) // Visual Studio needs annotating exported functions with this
#else
#define EXPORT_API // XCode does not need annotating exported functions, so define is empty
#endif

extern "C" {
    #define BUF_MAX_SIZE 4096
    #pragma pack(1)
    typedef struct Packet {
        int size;
        char data[BUF_MAX_SIZE];
        Packet() {
            memset(this, 0, sizeof(Packet));
        }
    }Packet;
    #pragma pack()
}
#endif//

