 #ifndef THREAD_H
 #define THREAD_H

#ifdef _WIN32
 #include <windows.h>
#else
#include "pthread.h"
#endif

 #define CLASS_UNCOPYABLE(classname)  private: classname(const classname&); classname& operator=(const classname&);

 struct Runnable {
     virtual void run() = 0;
     virtual ~Runnable() {}
 };

 class Thread : public Runnable {
     CLASS_UNCOPYABLE(Thread)
 public:
     explicit Thread(Runnable *target = 0);
	 virtual ~Thread();
     virtual void run() {}
     void start();
     void join();
#ifdef _WIN32
 private:
     static unsigned __stdcall threadProc(void *param);
 private:
     HANDLE _handle;
#else
 	pthread_t _handle;
 	static void*  threadProc(void *param);
#endif
     Runnable *_target;
 };

#endif/*THREAD_H*/



 //#include "Thread.h"
 //struct MyThread: public Thread {
 //    virtual void run() {
 //        for (int i = 0; i < 5; ++i) {
 //            cout << "MyThread Running..." << i << endl;
 //            threadSleep(100);
 //        }
 //    }
 //};

 //struct MyRunnable: public Runnable {
 //    virtual void run() {
 //        for (int i = 0; i < 5; ++i) {
 //            cout << "MyRunnable Running..." << i << endl;
 //            threadSleep(300);
 //        }
 //    }
 //};


 //thread->start();