/*
 * utils.h
 *
 *  Created on: 2015-7-23
 *      Author: Administrator
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <string>
using std::string;
#include "common.h"


typedef struct Buf{
	int index;
	char data[BUF_MAX_SIZE];
}Buf;

#define MAXSIZE_FILEPATH 1024
#define MAXSIZE_FILEDATA 4194304//1024*1024*4

extern "C" void threadSleep(int millSecond);
extern "C" void host2Ip(const char * url,char * ip,int iplength);
//extern "C" int readable_timeo(int fd, int sec);


extern "C" long long  systemCurrentTime(void);

//extern "C" long long  getFileSize(char* filePath);
//extern "C" int readFileAtPostion(FILE * file,char * outbuf,int pos,int len);
//extern "C" int writeFileAtPostion(char* filePath,char * inbuf,int pos,int len);
//extern "C" char* conactString(char *s1, char *s2);

//extern "C" int readFile(char * outbuf,int fileSize,int filePathSize,char* filePath);
//extern "C" int writeFile(char * inbuf,int fileSize,int filePathSize,char* filePath);

//url decode
//std::string UrlDecode(const std::string& szToDecode);

extern "C" {
EXPORT int openLogTxt();
EXPORT void closeLogTxt();
}

char* g_read_string_from_file(const char * _file_path, int &_size);


#endif /* UTILS_H_ */
