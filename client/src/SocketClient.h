#ifndef SOCKETCLIENT_H_
#define SOCKETCLIENT_H_
#include <cstdlib>
#include <iostream>
#include <queue>
using namespace std;

#include "utils.h"
#include "common.h"
#include "SocketBase.h"
#include "SocketHandler.h"
#include "SocketChannel.h"

class SocketClient {
private:
	queue<Packet*> packetQueue;
public:
	SocketChannel* mSocketChannel;
	SocketHandler* mSocketHandler;
	SocketClient(SocketHandler *pSocketHandler,const char* host,int port,int recvTimeout);
	virtual ~SocketClient();
	int recv();
	int recv(handlePacket handler);
	//返回发送完成的字节数,<=0则为失败
	int send(char* buf,int size);
	int send(char* buf,int size,int cmd);
	void handle(handlePacket handler);
	virtual void heartbeat();
};

#endif /* SOCKETCLIENT_H_ */
