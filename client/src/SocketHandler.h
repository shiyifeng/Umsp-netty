#ifndef SOCKETHANDLER_H_
#define SOCKETHANDLER_H_
#include "common.h"
#include "SocketChannel.h"
class SocketHandler{
public:

	SocketHandler();
	virtual ~SocketHandler();
	//链接成功
	virtual void onConnect(int socketfd) ;
	//失去链接
	virtual void ondisConnect(int socketfd);
	//接收收据
	virtual int doDecode(SocketChannel* socketChanel, char *buf, int size);

	virtual  int doEncode(int socketfd, char *buf, int size,int cmd,char* outBuf,int outBufSize);
	//发送数据 返回写入成功的字节数
	virtual int send(char* buf,int size);
	virtual void onErr(int socketfd,int errcode);
public:

};

#endif /* SOCKETHANDLER_H_ */
