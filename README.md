# Umsp-netty

### Description
Message push(synchronize) and Instant Messaging,base on netty

分布式数据分发，群组广播，网络数据传输框架，适用于视频通话，Moba等实时联网游戏场景

### Case

游戏方面的应用
- 泡泡堂(MOBA系列) (https://gitee.com/geliang/bomb-client)
- H5传奇(MMORPG系列) (https://github.com/fluttergo/mir2-client-js)

![泡泡堂](http://fluttergo.com/Game/BombBoy.assets/GIF.gif)

![H5传奇](https://gitee.com/geliang/PK/raw/master/ScreenShot_ChrSel.gif)
### Software Architecture

CS结构
- 服务端为Java，基于NIO框架Netty
- 客户端为C/C++、Java、C#，JavaScript、TypeScirpt。跨平台适配，支持Andoid，iOS，Win、Mac、Linux，H5平台

服务端架构图

![服务端架构图](server/doc/umsp.png)





### Feature

1. 性能不错, 可动态扩容, 理论上只要机器够,支持海量的玩家同时在线进行频繁的数据交互,如MOBA游戏
2. 可同时支持 私有tcp/websocket/kcp协议. 适用于H5/Native游戏二次开发
3. 玩家匹配服务独立, 匹配规则自定义,甚至可将客户端链接调度到localhost的机器上,方便调试.
4. 编译部署方便, 配合idea一键打包, 4个jar包走天下(4个服务可分开部署)

### Installation

1. 服务端工程用Idea或者Eclipse打开。本地依赖,可离线运行
2. 客户端Android Studio、WebStrom，CLion，VS
3. JS的Demo为JavaScipt所写Html页面，用于快速开发调试新功能，及并发测试。

### RoadMap

1. Match服务以脚本形式的拓展性改进,主要针对RoomService的调度规则 v3.0
2. 面对突发流量的弹性伸缩的服务能力,自动化运维免告警 v4.0

### Contribution
非常欢迎参与创作
1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request