package gl.java.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.demo.common.model.Goods;

public class GoodsReader {
	public static final HashMap<String, String> key2key = new HashMap<String, String>();
	
//	名称：赣南脐橙
//	分类：0
//	副标题：刚刚被太阳亲吻过
//	内容：丰富的维C，酸甜的味道，光是想想就不要不要了，再加上它还有补血和增强抵抗力的效果，哈哈~~
//	信息：6个装      1.2kg起
//	价格：30000
//	下架：0
//	图片：/img/goods/qicheng.jpg

	
	static{
		key2key.put("", "id");
		key2key.put("", "discount");
		key2key.put("", "contexturl");
		key2key.put("", "count");
		key2key.put("", "countremaining");
		key2key.put("", "limitbuycount");
		key2key.put("", "fplaceid");
		key2key.put("名称", "name");
		key2key.put("分类", "fgoodstype");
		key2key.put("价格", "price");
		key2key.put("副标题", "subtitle");
		key2key.put("信息", "mass");
		key2key.put("下架", "isalive");
		key2key.put("图片", "img");
		key2key.put("内容", "context");
	}

	public static void main(String[] args) throws IOException {
		String goodtxtPath = "C:\\Users\\Administrator\\Desktop\\goods.txt";
//		String goodtxtPath = "C:\\Users\\Administrator\\Desktop\\anaconda-ks.cfg";
//		System.out.println(StringUtils.getCharset(goodtxtPath));
		if (args != null && args.length >= 1) {
			goodtxtPath = args[0];
		}
		
		parseGoodsTxt(goodtxtPath);
	}



	private static void parseGoodsTxt(String goodtxtPath) throws IOException {
		byte b= (byte) 0xFF;
		System.out.println(b);
		System.out.println((char)b);
		System.out.println((int)((char)b));
		System.out.println((int)b);
		
		System.out.println("(byte)0xFF>>4 = "+ ((byte)0xFF>>4));
		System.out.println("0xFF>>4 = "+ (0xFF>>4));
		System.out.println(ByteUtils.int2byte(16711937));
		System.out.println(ByteUtils.int2byte2(16711937));
		byte bytes[] = {(byte)0x00,(byte)0xff,0x01,0x01};
		System.out.println(ByteUtils.byte2Int(ByteUtils.int2byte(-2)));
		System.out.println(ByteUtils.byte2Int(bytes));
		System.out.println(ByteUtils.byte2Int3(bytes,0));
		BufferedReader br = new BufferedReader(StringUtils.openStringFile(goodtxtPath));
		String line = "";
		ArrayList<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
		HashMap<String, Object> item = new HashMap<String, Object>();
		boolean isNewItem = false;// 是否应该换新的项
		while ((line = br.readLine()) != null) {
			if (StringUtils.isEmpty(line)) {
				continue;
			}
			String splt[] = line.trim().split("：");
			String key = splt[0];
			if (key2key.containsKey(key)) {
				key = key2key.get(key);
			}
			if (StringUtils.isEmpty(key)) {
				System.err.println("null key");
				continue;
			}
			if (item.get(key) != null) {
				System.out.println("==new item==");
				isNewItem = true;
			}
			if (isNewItem) {
				list.add(item);
				item = new HashMap<String, Object>();
			}

			String value = splt[1];
			if (StringUtils.isEmpty(value)) {
				splt[1] = "";
				System.err.println(key + ": null value");
			}
			item.put(key, value.trim());
			System.out.println(String.format("%s:%s", key, value));

			if (isNewItem) {
				isNewItem = false;
			}
		}
		list.add(item);
		for (int i = 0; i < list.size(); i++) {
			HashMap<String, Object> map = list.get(i);
			Object[] set=  (Object[]) (map.keySet().toArray());
			Goods goods = new Goods();
			for (int j = 0; j < set.length; j++) {
				goods.set(set[j].toString(), map.get(set[j].toString()));
			}
			System.out.println("------- " + i + " -------");
			System.out.println(goods);
			goods.save();
		}
		br.close();
	}

}
