package com.demo.controls;

import java.util.Map;

import com.demo.common.model.Goods;
import com.jfinal.plugin.activerecord.Page;

/**
 * IndexController
 */
public class GoodsController extends BaseController {
	public void index() {
		Map<String,String[]> map = getParaMap();
		if(map.size()<=0){
			renderText("What kind of goods do you want?");
			return;
		}
		int pageindex = getParaToInt("pageindex",1);
		int pageSize= getParaToInt("pagesize",10);
		Page<Goods> result = Goods.dao.paginate(pageindex, pageSize,
				"select *",
				"from goods where fgoodstype = ?  order by id desc", getPara("type"));
		renderJson(result.getList());
	}
}