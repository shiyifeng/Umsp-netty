package com.demo.controls;

import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.demo.common.model.Calendar;
import com.demo.common.model.Goods;
import com.jfinal.kit.JsonKit;

/**
 * IndexController
 */
public class CalendarController extends BaseController {
	/**
	 * 活动推荐,在单独的recommand表中,以提高性能
	 */
	public void index() {
		List<Goods> users = Goods.dao.find("select * from goods where fgoodstype = ? order by id desc",getPara("type"));
		renderJson(users);
	}
	public void layout() {
//		starttime=1466318347859&endtime=1466318547859
		long starttime = getParaToLong("starttime",0l);		
		long endtime = getParaToLong("endtime",0l);	
		if (starttime==0l||endtime==0l) {
			renderErr(ERR_ILLPAR,"starttime || endtime is 0");
			return;
		}
		List<Calendar> users = Calendar.dao.find("select * from calendar where fgoodstype = ? and data > ?",getPara("type"),starttime);
		JSONArray ja = JSONArray.parseArray(JsonKit.toJson(users));
		for (int i = 0; i < ja.size(); i++) {
			JSONObject jsonObject = ja.getJSONObject(i);
			jsonObject.put("goods",getGoods(jsonObject.getIntValue("fgoodsid")));
		}
		renderJson(ja);
	}
	
}