package com.demo.admin;

import javax.xml.ws.Action;


import com.demo.common.model.Goods;
import com.demo.controls.BaseController;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Page;

public class EditGoodsController extends BaseController {
	 public void index() {
	        Page<Goods> paginate = Goods.dao.paginate(getParaToInt(0, 1).intValue(), 30, "select * from goods  ","");
			setAttr("goodsPage", paginate);
	        render("goods.html");
	    }
	    
	    public void add() {
	    	
	    }
	    
	    @Before(GoodsValidator.class)
	    public void save() {
	        getModel(Goods.class).save();
	        redirect("/admin/goods");
	    }
	    
	    public void edit() {
	        setAttr("goods",Goods.dao.findById(getParaToInt()));
	    }
	    @Action
	    @Before(GoodsValidator.class)
	    public void update() {
	        getModel(Goods.class).update();
	        redirect("/admin/goods");
	    }
	    
	    public void delete() {
	    	Goods.dao.deleteById(getParaToInt());
	        redirect("/admin/goods");
	    }
}