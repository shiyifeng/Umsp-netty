package com.demo.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BasePayment<M extends BasePayment<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}

	public java.lang.Integer getId() {
		return get("id");
	}

	public void setOrderids(java.lang.String orderids) {
		set("orderids", orderids);
	}

	public java.lang.String getOrderids() {
		return get("orderids");
	}

	public void setFuserid(java.lang.Integer fuserid) {
		set("fuserid", fuserid);
	}

	public java.lang.Integer getFuserid() {
		return get("fuserid");
	}

	public void setCreatetime(java.util.Date createtime) {
		set("createtime", createtime);
	}

	public java.util.Date getCreatetime() {
		return get("createtime");
	}

	public void setEndtime(java.util.Date endtime) {
		set("endtime", endtime);
	}

	public java.util.Date getEndtime() {
		return get("endtime");
	}

	public void setIspay(java.lang.Integer ispay) {
		set("ispay", ispay);
	}

	public java.lang.Integer getIspay() {
		return get("ispay");
	}

	public void setPaytype(java.lang.Integer paytype) {
		set("paytype", paytype);
	}

	public java.lang.Integer getPaytype() {
		return get("paytype");
	}

	public void setTotalprice(java.lang.Integer totalprice) {
		set("totalprice", totalprice);
	}

	public java.lang.Integer getTotalprice() {
		return get("totalprice");
	}

	public void setPayinfo(java.lang.String payinfo) {
		set("payinfo", payinfo);
	}

	public java.lang.String getPayinfo() {
		return get("payinfo");
	}

	public void setCallback(java.lang.String callback) {
		set("callback", callback);
	}

	public java.lang.String getCallback() {
		return get("callback");
	}

	public void setThirdcallbackinfo(java.lang.String thirdcallbackinfo) {
		set("thirdcallbackinfo", thirdcallbackinfo);
	}

	public java.lang.String getThirdcallbackinfo() {
		return get("thirdcallbackinfo");
	}

}
