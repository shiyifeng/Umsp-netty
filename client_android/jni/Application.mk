TARGET_CPU_API := armeabi-v7a
APP_ABI := armeabi-v7a
#APP_STL := gnustl_shared
NDK_TOOLCHAIN_VERSION := 4.8

APP_PLATFORM = android-9 
APP_STL = gnustl_static

APP_CPPFLAGS += -frtti
APP_CPPFLAGS += -fexceptions
APP_CPPFLAGS += -std=c++11


