#bash 
# cd jni
# ndk-build

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := umsp-client

##########
SDK_PATH := $(LOCAL_PATH)/../../client

LOCAL_SRC_FILES := $(wildcard $(LOCAL_PATH)/jni/*.c)
LOCAL_SRC_FILES += $(wildcard $(LOCAL_PATH)/jni/*.cpp)
LOCAL_SRC_FILES += $(wildcard $(SDK_PATH)/src/*.cpp)
LOCAL_SRC_FILES += $(wildcard $(SDK_PATH)/src/*.c)
###########
LOCAL_C_INCLUDES := $(SDK_PATH)/src

##########
LOCAL_CPPFLAGS :=  -fexceptions
LOCAL_CPP_FEATURES += rtti
LOCAL_CFLAGS +=  -DDEBUG

###########
LOCAL_LDLIBS += -llog  -pthread
include $(BUILD_SHARED_LIBRARY)
