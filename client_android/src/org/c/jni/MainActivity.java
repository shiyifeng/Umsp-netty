package org.c.jni;


import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.client_android.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);
	System.loadLibrary("JniHelper");
	JniUtil.initJniEnvironment();
	new Thread(){
	    public void run() {
		try {
		    Thread.sleep(1000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		JniUtil.test();
	    };
	}.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == R.id.action_settings) {
	    return true;
	}
	return super.onOptionsItemSelected(item);
    }
    public static void  NativeCall(final long pCallBack,final int result){
	System.out.println("NativeCall "+result+" pCallBack:"+pCallBack);
	new Thread(){
	    public void run() {
		try {
		    Thread.sleep(3000);
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}
		JniUtil.callbackJniString(pCallBack, result, "hello中文字段{}", "hello中文字段{}".length());
	    };
	}.start();
    }
    public static void  NativeCall(int result){
	System.out.println("NativeCall "+result);
    }
    public static void  NativeCall(long pb){
	System.out.println("NativeCall pb:"+pb);
    }
}
