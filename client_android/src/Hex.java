
public class Hex {
    final static char[] TABLE = { '0', '1', '2', '3', '4', '5', '6', '7', '8',
	    '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
	    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
	    'z' };

    public static String bytesToString(byte[] src) {
	final int length = src.length;
	final int dstlength = length * 2;
	char[] buf = new char[dstlength];

	for (int i = 0; i < length; i++) {
	    buf[i * 2] = TABLE[(src[i] & 0xFF) >>> 4];// H
	    buf[i * 2 + 1] = TABLE[((src[i] & 0x0F)) & 0xFF];// L

	}
	return new String(buf, 0, dstlength);
    }

    public static void stringToBytes(String src, byte[] out) {
	if (out.length * 2 < src.length()) {
	    throw new IllegalArgumentException("out.length*2<src.length()");
	}
	char[] chars = src.toCharArray();
	for (int i = 0; i < out.length; i++) {
	    int h = 0;
	    int l = 0;
	    for (int j = 0; j < TABLE.length; j++) {
		if (chars[i * 2] == TABLE[j]) {
		    h = j;
		    break;
		}
	    }
	    for (int j = 0; j < TABLE.length; j++) {
		if (chars[i * 2 + 1] == TABLE[j]) {
		    l = j;
		    break;
		}
	    }
	    out[i] = (byte) ((((h << 4 | l & 0x0F))) & 0xFF);
	}
    }
}
