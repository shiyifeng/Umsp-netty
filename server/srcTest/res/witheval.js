
var a;

function outer(p, e) {
    eval(e);
    function inner() {
        a = 1;
        c = 10;
        if (a !== 1) {
            throw new Error("a !== 1");
        }
        if (b !== 3) {
            throw new Error("b !== 3");
        }
        if (c !== 10) {
            throw new Error("c !== 10");
        }
    }
    inner();
}

outer({}, "b = 3;");

if (a !== 1) {
    throw new Error("a !== 1");
}
if (b !== 3) {
    throw new Error("b !== 3");
}
if (c !== 10) {
    throw new Error("c !== 10");
}