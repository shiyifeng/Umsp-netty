package gl.java.umsp.bean;

import gl.java.umsp.ErrCode;

public class MatchResult extends JsonBean{
    public boolean isSuccess;
    public Room room;
    public String errInfo;
    public int errCode;
    public User wantToMatchUser;

    public MatchResult(ErrCode errCode) {
        this.errCode = errCode.getCode();
        this.errInfo = errCode.getDescribe();
        this.isSuccess = false;
    }


    public MatchResult(Room room) {
        this.room = room;
        this.isSuccess =true;
    }



    public static MatchResult createFailMatchResult(ErrCode errCode,int userID) {
        MatchResult matchResult = new MatchResult(errCode);
        matchResult.wantToMatchUser = new User(userID);
        return matchResult;
    }
    public static MatchResult createSuccessMatchResult(Match match, Room room) {
        MatchResult matchResult = new MatchResult(room);
        matchResult.wantToMatchUser = match.wantToMatchUser;
        return matchResult;
    }

}
