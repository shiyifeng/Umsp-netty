package gl.java.umsp.bean;

import com.google.gson.Gson;
import gl.java.umsp.UmspHeader;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.nio.channels.Channel;

/**
 * 服务器用户
 */
@Data
public class User extends JsonBean {

    /**
     * 匹配的房间名字
     */
    public String roomName;
    /**
     * 匹配的房间密码
     */
    public String roomPassWord;
    /**
     * 匹配携带的额外的信息,用于拓展
     */
    public String matchExtInfo;

    public User(int userID, int gameID, String nickName) {
        this.userID = userID;
        this.gameID = gameID;
        this.nickName = nickName;
    }

    public transient Channel channel;
    public int userID;
    public int gameID;
    public String nickName;
    /**
     * 权限标识,如会员登录,保留字段
     */
    public long accessFlag;


    /**
     * 玩家在房间内的状态
     *
     * @see RoomPlayerState {@link RoomPlayerState }
     */
    public int roomPlayerState;
    public String avatar;
    public String token;
    public String roomSession;
    /**
     * 分组ID,组队匹配得到;
     */
    public int groupID;
    /**
     * 匹配值
     */
    public int mmr;
    /**
     * 匹配方式
     */
    public int matchType;


    public User(int userID) {
       this.userID = userID;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public static User formJson(String message) {
        return new Gson().fromJson(message, User.class);
    }


    public static User creatUser(UmspHeader msg) {
        User user = new User(msg.userID, msg.gameID, "");
        user.gameID = msg.gameID;
        user.roomPlayerState = RoomPlayerState.UNREADY;
        return user;

    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof User && userID == (((User) obj).userID);

    }

    public static User getUserInfo(Integer userID) {
        return new User(userID);
    }
}
