package gl.java.umsp.bean;

import gl.java.util.JsonUtil;

public class JsonBean {
    public String toString(){
        return JsonUtil.toString(this);
    }

    public static <T> T   fromJson(String s,Class<T> clazz) {
        return JsonUtil.fromJson(s,clazz);
    }
}
