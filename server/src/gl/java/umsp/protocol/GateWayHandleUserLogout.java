package gl.java.umsp.protocol;

import gl.java.umsp.Umsp;
import gl.java.umsp.UmspHeader;
import io.netty.channel.ChannelHandlerContext;

public class GateWayHandleUserLogout implements IUmspHandler {
    @Override
    public boolean handle(ChannelHandlerContext ctx, UmspHeader msg) {
        if (Umsp.CMD_LOGOUT == msg.cmd){
            ctx.close();
            return true;
        }
        return false;
    }
}
