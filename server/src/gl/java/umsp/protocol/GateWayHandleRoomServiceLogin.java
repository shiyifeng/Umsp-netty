package gl.java.umsp.protocol;

import gl.java.umsp.Umsp;
import gl.java.umsp.UmspConfig;
import gl.java.umsp.UmspHeader;
import gl.java.umsp.gateway.ProxyRoomServiceConnectionPool;
import gl.java.umsp.room.RoomService;
import gl.java.umsp.room.RoomServiceConfig;
import gl.java.util.JsonUtil;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GateWayHandleRoomServiceLogin implements IUmspHandler {
    @Override
    public boolean handle(ChannelHandlerContext ctx, UmspHeader msg) {
        if (Umsp.CMD_ROOM_SERVICE_LOGIN == msg.cmd) {
            RoomServiceConfig roomService = JsonUtil.fromJson(new String(msg.payload), RoomServiceConfig.class);
            ProxyRoomServiceConnectionPool.getInstance().put(this.getKey(roomService), ctx.channel(), roomService);
            log.info("RoomService login: " + roomService);
            Umsp.returnEmpty(ctx.channel(), Umsp.CMD_ROOM_SERVICE_LOGIN_RSP);
            return true;
        }
        return false;
    }

    public static String getKey(RoomServiceConfig roomService) {
        return roomService.gameID + UmspConfig.LINK_CHAR_KEY + roomService.serverIndex;
    }


}
