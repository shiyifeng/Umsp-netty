package gl.java.umsp.event;

import gl.java.mq.Consumer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EventSubscriber {


    public static void registerAsyncSubscriberCallBack(final IEventSubscriber eventSubscriber, final String... channel) {
        new Consumer(EventServerConfig.ServerUrl, EventServerConfig.ServerPort, new Consumer.Receiver() {
            @Override
            public void onMessage(String topic, String msg) {
                eventSubscriber.onMessage(topic, msg);
            }
        }, channel).start();

    }


}