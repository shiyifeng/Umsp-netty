package gl.java.umsp.event;

import org.json.JSONException;
import org.json.JSONObject;

public class Event {
    public Event(String eventName, Object eventValue) {
        this.eventName = eventName;
        this.eventValue = eventValue;
    }

    /**
     * 房间服务器上线事件
     */
    public final static String ROOM_SERVICE_ON_LINE = "ROOM_SERVICE_ON_LINE";
    /**
     * 房间服务器下线事件
     */
    public final static String ROOM_SERVICE_OFF_LINE = "ROOM_SERVICE_OFF_LINE";
    public final static String GATEWAY_SERVICE_ON_LINE = "GATEWAY_SERVICE_ON_LINE";
    public final static String GATEWAY_SERVICE_OFF_LINE = "ROOM_SERVICE_OFF_LINE";
    public static final String CHANNEL_MATCH_EVENT = "CHANNEL_MATCH_EVENT";
    public static final String CHANNEL_MATCH_EVENT_RESULT = "CHANNEL_MATCH_EVENT_RESULT";
    public static final String ERR = "ERR";

    public static final String USER_OFF_LINE = "USER_OFF_LINE";
    public static final String USER_ENTER_ROOM = "USER_ENTER_ROOM";
    public static final String USER_EXIT_ROOM = "USER_EXIT_ROOM";

    public static String buildUserEvent(int userID) {
        JSONObject jo = new JSONObject();
        try {
            jo.put("userID", userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jo.toString();
    }

    public static Event buildEvent(String key, int userID) {
        return new Event(key, userID);
    }

    public String eventName = "";
    public Object eventValue = "";
    /**
     * 后面跟_gameId
     */
    public static final String RoomRecycleEvent_ = "RoomRecycleEvent_";

}
