package gl.java.umsp.match;

import gl.java.umsp.IDistributeService;
import gl.java.util.PropertiesUtil;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;

/**
 * 匹配服务器配置
 */
@EqualsAndHashCode(callSuper = false)
@Data()
public class MatchServiceConfig extends IDistributeService.Config {
    /**
     * 匹配服务器所属的游戏ID,默认为0,做为保底匹配服务器,网管会在没有其他匹配服务器时,使用gameID为0的匹配服务器做匹配.
     */
    private int gameID = 0;
    private int serviceIndex = 0;
    /**
     * 这台服务器支持的匹配方式列表
     */
    public static ArrayList<IMatch> SupportMatchTypeList = new ArrayList<IMatch>();

    private MatchServiceConfig() {
        SupportMatchTypeList.clear();
        SupportMatchTypeList.add(new QuickMatch());
        SupportMatchTypeList.add(new SameNameMatch());
    }

    private static class Instance {
        public static MatchServiceConfig defaultConfig = new MatchServiceConfig();
    }

    public static MatchServiceConfig getDefaultConfig() {
        return Instance.defaultConfig;
    }

    public String toString() {
        return super.toString();
    }
}
