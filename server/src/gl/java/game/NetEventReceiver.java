package gl.java.game;


public interface NetEventReceiver {
    void onReceiver(NetEvent netEvent);
}
