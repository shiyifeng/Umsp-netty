package gl.java.mq;

public class Packet {
    String topic;
    String msg;

    public Packet(String topic, String umspHeader) {
        this.topic = topic;
        this.msg = umspHeader;
    }

    @Override
    public String toString() {
        return "["+topic+"] "+msg;
    }
}
