package gl.java.mq;

import gl.java.umsp.UmspHeader;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Test {

    public static void main(String[] args) {
//        new MessageQueueService(11111).start();
        //
        Consumer consumer = new Consumer("118.24.53.22", 11111, new Consumer.Receiver() {
            @Override
            public void onMessage(String topic, String msg) {
                System.out.println(msg);
            }
        }, "MQ-Topic-1", "test");
        consumer.start();
        //
        Producer p = new Producer("118.24.53.22", 11111);
        p.start();
        for (int i = 0; i < 10; i++) {
            p.publish("test", String.valueOf(i));
        }
//        for (int i = 0; i < 100000; i++) {
//            p.publish("MQ-Topic-1", String.valueOf(i));
//        }
    }
}
