package gl.java.mq;

import gl.java.umsp.Umsp;
import gl.java.umsp.UmspHeader;
import gl.java.util.JsonUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NettyMsgHelper {
    private static Logger log = LoggerFactory.getLogger(Consumer.class);
    public static void sendPublishMsg(Packet p, Channel c) {
//        log.info("Publish: " + p);
        Umsp.returnStringMsg(Umsp.CMD_PUBLISH, JsonUtil.toString(p), c);
    }

    public static void buildSubMsg(ChannelHandlerContext ctx, JSONArray object) {
//        log.info("Subscribe: " + object);
        UmspHeader msg = new UmspHeader(Umsp.CMD_SUBSCRIBE, object.toString());
        Umsp.writeAndFlush(ctx.channel(), msg);
    }
}
